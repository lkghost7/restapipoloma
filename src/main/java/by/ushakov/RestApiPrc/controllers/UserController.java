package by.ushakov.RestApiPrc.controllers;

import by.ushakov.RestApiPrc.models.PhoneFileDTO;
import by.ushakov.RestApiPrc.models.UserEntityDTO;
import by.ushakov.RestApiPrc.models.UserEntitySaveDTO;
import by.ushakov.RestApiPrc.models.YarikDTO;
import by.ushakov.RestApiPrc.services.UserService;
import by.ushakov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@Validated
@RestController
@RequestMapping("/Poloma")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping(value = "/getUserBy")
    public ResponseList<UserEntityDTO> getUserBy(UserEntityDTO vTestPrcGet) throws SQLException {
        return userService.getUserBy(vTestPrcGet);
    }

    @PostMapping(value = "/saveUser", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseList<UserEntitySaveDTO> saveUser(@RequestBody String json) {
        return userService.saveUser(json);
    }

    // http://localhost:8080/vinty/deleteUser?id=5
    @DeleteMapping(value = "/deleteUser", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseList<UserEntityDTO> deleteUser(@RequestParam Long id) {
        return userService.deleteUser(id);
    }

    @GetMapping(value = "/getBD")
    public ResponseList<YarikDTO> getYarik() throws SQLException {
        return userService.getListUser();
    }

    @GetMapping(value = "/getFile")
    public List<PhoneFileDTO> getFile() throws SQLException {
        return userService.getFile();
    }
}




