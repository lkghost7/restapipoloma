package by.ushakov.RestApiPrc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class YarikDTO {

    @Id
    private Long id;
    private String name;
    private Long age;
    private String message;
}
