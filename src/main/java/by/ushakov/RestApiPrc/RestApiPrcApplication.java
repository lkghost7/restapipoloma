package by.ushakov.RestApiPrc;

import lombok.extern.java.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@Log
@PropertySource(value = "classpath:application.properties")
@ConfigurationPropertiesScan
public class RestApiPrcApplication {
	public static void main(String[] args) {
		SpringApplication.run(RestApiPrcApplication.class, args);
		log.info("-------------------------------------------------------------------------------");
		log.info("----------------------USER DIRECTORY SERVICE STARTED!-----------------------");
		log.info("-------------------------------------------------------------------------------");
	}
}
