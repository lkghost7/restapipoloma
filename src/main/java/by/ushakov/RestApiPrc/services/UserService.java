package by.ushakov.RestApiPrc.services;

import by.ushakov.RestApiPrc.dao.UserDAO;
import by.ushakov.RestApiPrc.models.PhoneFileDTO;
import by.ushakov.RestApiPrc.models.UserEntityDTO;
import by.ushakov.RestApiPrc.models.UserEntitySaveDTO;
import by.ushakov.RestApiPrc.models.YarikDTO;
import by.ushakov.RestApiPrc.utils.JsonHelper;
import by.ushakov.RestApiPrc.utils.ResponseList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

@Log4j2
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserDAO userDao;
    private final JsonHelper jsonHelper;

    @Transactional(readOnly = true)
    public ResponseList<UserEntityDTO> getUserBy(UserEntityDTO userEntityDTO) {
        return userDao.getUserBy(userEntityDTO);
    }

    @Transactional
    public ResponseList<UserEntitySaveDTO> saveUser(String json) {
        if (Objects.nonNull(json) && jsonHelper.isValid(json)) {
            ObjectMapper objectMapper = new ObjectMapper();

            UserEntitySaveDTO user = new UserEntitySaveDTO();
            try {
                user = objectMapper.readValue(json, UserEntitySaveDTO.class);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            return userDao.saveUser(UserEntitySaveDTO.builder()
                            .code(user.getCode())
                            .cityId(user.getCityId())
                            .address(user.getAddress())
                            .location(user.getLocation())
                            .networkOperatorId(user.getNetworkOperatorId())
                            .workSchedule(user.getWorkSchedule())
                            .phoneNumber(user.getPhoneNumber())
                            .chief(user.getChief())
                    .build());
        }
        log.error("JSON: " + json, () -> new RuntimeException("JSON is NOT valid!"));
        return ResponseList.<UserEntitySaveDTO>builder()
                .respCode("ERROR")
                .respMess("JSON is NOT valid!")
                .build();
    }

    @Transactional
    public ResponseList<UserEntityDTO> deleteUser(Long id) {
        return userDao.deleteUser(id);
    }

    public ResponseList<YarikDTO> getListUser () {
        List<YarikDTO> yarikList = new ArrayList<>(); //1
        YarikDTO yarik = YarikDTO.builder().name("YArik").age(44L).message("Have").build();
        YarikDTO yarik2 = YarikDTO.builder().name("YArik").age(44L).message("Have").build();
        YarikDTO yarik3 = YarikDTO.builder().name("YArik").age(44L).message("Have").build();
        YarikDTO yarik4 = YarikDTO.builder().name("YArik").age(44L).message("Have").build();
        yarikList.add(yarik);
        yarikList.add(yarik2);
        yarikList.add(yarik3);
        yarikList.add(yarik4);

        ResponseList<YarikDTO> responseList = new ResponseList<>();
        responseList.setContent(yarikList);
        return responseList;
    }

    public List<PhoneFileDTO> getFile () {

        List<String> lines = new ArrayList<>();
        List<PhoneFileDTO> fileDto = new ArrayList<>();
        try {
            File file = new File("D:\\JavaProjects\\phone2.txt");
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                lines.add(line);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден: " + e.getMessage());
        }

        for (int i = 0; i < lines.size(); i++) {
            String name = lines.get(i);
            PhoneFileDTO user = new PhoneFileDTO();
            user.setName(name);
            user.setId(Long.valueOf(i));
            fileDto.add(user);
        }

        lines.forEach(s -> {
            System.out.println(s);
        });

        System.out.print("!!!! -------------- get File ------------- !!!!");
        return  fileDto;
    }
}






