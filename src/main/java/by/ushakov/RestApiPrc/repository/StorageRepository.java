package by.ushakov.RestApiPrc.repository;

import by.ushakov.RestApiPrc.models.UserEntityDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StorageRepository extends JpaRepository<UserEntityDTO, Long>, PagingAndSortingRepository<UserEntityDTO, Long> {



}
