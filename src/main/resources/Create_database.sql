--drop table user_directory.user_entity ;
--drop table user_directory.phones ;
--drop table user_directory.city  ;
--drop table user_directory.network_operator ;

CREATE TABLE user_directory.network_operator (
	id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	network_operator_name varchar(100));

CREATE TABLE user_directory.city (
	id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	city_name varchar(100));

CREATE TABLE user_directory.phones (
	id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	phone_number varchar(20));

CREATE TABLE user_directory.user_entity (
	id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	code varchar,
	city_id bigint,
	address varchar,
	"location" varchar,
	network_operator_id bigint,
	work_schedule varchar,
	phone_id bigint,
	chief varchar,
	operator_id bigint,
	CONSTRAINT user_entity_city_id_fk FOREIGN KEY (city_id) REFERENCES user_directory.city(id),
	CONSTRAINT user_entity_phones_id_fk FOREIGN KEY (phone_id) REFERENCES user_directory.phones(id),
    CONSTRAINT user_entity_network_operator_id_fk FOREIGN KEY (network_operator_id) REFERENCES user_directory.network_operator(id));
   

----------------------------------------------------------
--   Procedures
----------------------------------------------------------
   
   CREATE OR REPLACE FUNCTION user_directory.get_users()
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare
    ref_cursor REFCURSOR := 'mycursor';
begin
    open ref_cursor for
select
	ue.id,
	ue.code,
	c.city_name,
	ue.address,
	ue."location",
	n.network_operator_name,
	ue.work_schedule,
	p.phone_number,
	ue.chief
from
	user_directory.user_entity ue
left join user_directory.city c on
	c.id = ue.city_id
left join user_directory.phones p on
	p.user_entity_id = ue.id
left join user_directory.network_operator n on
	n.id = p.user_entity_id;

return (ref_cursor);
end;
$function$
;

----------------------------------------------------------

create or replace
function user_directory.get_users_2(
    OUT p_resp_code varchar ,
    OUT ref_cursor REFCURSOR
) 
as $$
declare
	--p_resp_code text;
    --ref_cursor REFCURSOR := 'mycursor';

begin
	p_resp_code := 'OK';
    open ref_cursor for
select
	ue.id,
	ue.code,
	c.city_name,
	ue.address,
	ue."location",
	n.network_operator_name,
	ue.work_schedule,
	p.phone_number,
	ue.chief
from
	user_directory.user_entity ue
left join user_directory.city c on
	c.id = ue.city_id
left join user_directory.phones p on
	p.user_entity_id = ue.id
left join user_directory.network_operator n on
	n.id = p.user_entity_id;
end;
$$ language plpgsql;

-----------------------------------------------------------

create or replace
function user_directory.save_user(
	IN p_id BIGINT, 
    IN p_code VARCHAR(50),
    IN p_city_id BIGINT,
    IN p_address VARCHAR(200),
    IN p_location VARCHAR(200),
    IN p_network_operator_id BIGINT,
    IN p_work_schedule VARCHAR(200),
    IN p_phone VARCHAR(100),
    IN p_chief VARCHAR(200),
    OUT p_resp_code varchar ,
    OUT p_resp_message varchar
    --OUT ref_cursor REFCURSOR
) 
as $$
declare
	p_phone_id BIGINT;
    --ref_cursor REFCURSOR := 'mycursor';
begin
	
	
if p_id is null then
--вставка нового
    INSERT INTO user_directory.user_entity (
    code,
    city_id,
    address,
    "location",
    network_operator_id,
    work_schedule,
    chief) 
    VALUES (
    p_code,
    p_city_id,
    p_address,
    p_location,
    p_network_operator_id,
    p_work_schedule,
    p_chief)
    returning id into p_id;
    
   if p_phone is not null then
    insert into user_directory.PHONES (PHONE_NUMBER, USER_ENTITY_ID) 
    values (p_phone, p_id) 
    returning id into p_phone_id;
   
    update user_directory.USER_ENTITY set PHONE_ID = p_phone_id where id = p_id;
   	end if;
end if;
--    p_out_id := p_id;
   p_resp_code := 'OK';
   p_resp_message:= 'Строка сохранена успешно. id = ' || p_id;
   
end;
$$ language plpgsql;

-------------------------------------------------------------
-- Проверить работу процедур вручную из DBeaver

--SELECT user_directory.get_users_2();
--SELECT user_directory.SAVE_USER(:p_id, :p_code, :p_city_id, :p_address, :p_location, :p_network_operator_id, :p_work_schedule, :p_phone_id, :p_chief);

-- Дать на вход параметры нужного типа



